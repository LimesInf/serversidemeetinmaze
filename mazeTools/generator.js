let gridSize;
let grid = [];
let stack = [];
let data;
let currentCell;
module.exports = function (size) {
    data = [];
    gridSize = size;
    generate();
    return data;
};


function init() {
    for (let i = 0; i < gridSize; i++) {
        let row = [];
        for (let j = 0; j < gridSize; j++) {
            let cell = new Cell(i, j);
            row.push(cell);
        }
        grid.push(row);
    }
    currentCell = grid[0][0];
    currentCell.visited = true;
    data.push(currentCell);
}

function generate() {
    init();
    let counter = 0;
    while (data.length !== gridSize * gridSize) {
        let next = currentCell.neighbors();
        if (next) {
            next.visited = true;
            removeWalls(currentCell, next);
            stack.push(currentCell);
            currentCell = next;
            data.push(next)
        } else if (stack.length > 0) {
            currentCell = stack.pop();
        }
        counter++;
    }

}

function Cell(i, j) {
    this.i = i;
    this.j = j;
    this.walls = [true, true, true, true];
    this.visited = false;
    this.neighbors = function () {
        let neighbors = [];
        if (j > 0) {
            let top = grid[i][j - 1];
            checkNeighbor(top, neighbors);
        }
        if (i < gridSize - 1) {
            let right = grid[i + 1][j];
            checkNeighbor(right, neighbors);
        }
        if (j < gridSize - 1) {
            let bottom = grid[i][j + 1];
            checkNeighbor(bottom, neighbors);
        }
        if (i > 0) {
            let left = grid[i - 1][j];
            checkNeighbor(left, neighbors);
        }

        if (neighbors.length > 0) {
            let random = getRandomInt(neighbors.length);
            return neighbors[random];
        }
    }
}

function checkNeighbor(cell, neighbors) {
    if (cell && !cell.visited) {
        neighbors.push(cell);
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function removeWalls(firstCell, secondCell) {
    let x = firstCell.i - secondCell.i;
    let y = firstCell.j - secondCell.j;

    if (x === 1) {
        firstCell.walls[3] = false;
        secondCell.walls[1] = false;
    }
    else if (x === -1) {
        firstCell.walls[1] = false;
        secondCell.walls[3] = false;
    }
    if (y === 1) {
        firstCell.walls[0] = false;
        secondCell.walls[2] = false;
    }
    else if (y === -1) {
        firstCell.walls[2] = false;
        secondCell.walls[0] = false;
    }
}