module.exports = function () {
    var express = require('express');
    var router = express.Router();
    var usersControll = require('../helpers/usersControll');
    router.get('/users',function (req,res,next) {
       usersControll.getUsers(res);
    });
    return router;
};