var express = require('express');
var router = require('express-promise-router')();
const  usersControllers=require('../controllers/users');
const {validate,schemas}=require('../helpers/validator');
const passport=require('passport');
const passportConfig=require('../passport');

/* GET users listing. */
router.route('/signUp')
    .post(validate(schemas.authSchma),usersControllers.signUp);
router.route('/signIn')
    .post(validate(schemas.authSchma),passport.authenticate('local',{session:false}),usersControllers.singIn);
router.route('/secret')
    .get(passport.authenticate('jwt',{session:false}),usersControllers.getSecret);


module.exports = router;
