const User = require('../models/userModel');
const JWT = require('jsonwebtoken');
const {secret} = require('../configs/tokenSecretconfig');


generateToken = (User) => {
    return JWT.sign({
        iss: "limes",
        sub: User._id,
    }, secret);
};
module.exports = {
    signUp: async (req, res, next) => {
        const {email, password, nick, winCount, loseCount, drawCount, level, cash, onlineTime} = req.value.body;
        const userFromDatabase = await User.findOne({email});
        if (userFromDatabase) {
            return res.status(403).json({error: "Email is already used"});
        }
        const newUser = new User({email, password, nick, winCount, loseCount, drawCount, level, cash, onlineTime});
        await newUser.save();
        //generating token

        const token = generateToken(newUser);
        res.status(200).json({token});
    },

    singIn: async (req, res, next) => {
        const token = generateToken(req.user);
        res.status(200).json({token});
    },
    getSecret: async (req, res, next) => {
        res.json({user: req.user});
    }
};