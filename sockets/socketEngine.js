var io;
var playerSocket = require('./playerSocket');
const {gameEvens} = require('./socketConfig');
const generate = require('../mazeTools/generator');
const {mazeConfig, roomDifficultyConfig, checkConnection} = require('../mazeTools/mazeConfigs');
let gridDiff = mazeConfig[0];
let easyRoomConnection = 0;
let normalRoomConnection = 0;
let hardRoomConnrction = 0;
module.exports = {
    setIo: function (socketIO) {
        io = socketIO;
    },
    handleGameEvents: (socket) => {
        socket.on(gameEvens.EVENT_READY, function () {
            let maze = [];
            let data = generate(maze, gridDiff);
            socket.emit(gameEvens.EVENT_ROOM_FILLED, {size: gridDiff, data: data});
        });
        socket.on(gameEvens.EVENT_ROOM_CONNECTION, function (room) {
            socket.join(room);
            io.in(room).emit(gameEvens.EVENT_TEST, {msg: room});
        });

        socket.on(gameEvens.EVENT_DISCONNECT_FROM_ROOM, function (room) {
            console.log(room);
        });
    },
    handlePlayerEvents: (socket) => {
        playerSocket(socket);
    }
};