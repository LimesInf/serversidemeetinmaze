const players = [];
const {playerEvents} = require('./socketConfig');
module.exports = function (socket) {
    socket.emit(playerEvents.EVENT_GET_PLAYERS, players);
    socket.broadcast.emit(playerEvents.EVENT_NEW_USER, {id: socket.id});
    socket.on(playerEvents.EVENT_MOVED, function (data) {
        data.id = socket.id;
        socket.broadcast.emit(playerEvents.EVENT_MOVED, data);
        for (var i = 0; i < players.length; i++) {
            if (players[i].id === socket.id) {
                players[i].x = data.x;
                players[i].y = data.y;
            }
        }
    });
    socket.on('disconnect', function () {
        socket.broadcast.emit(playerEvents.EVENT_DISCONNECT, {id: socket.id});
        for (var i = 0; i < players.length; i++) {
            if (players[i].id === socket.id) {
                players.splice(i, 1);
            }
        }
    });
    players.push(new Player(socket.id, 0, 0));
};

function Player(id, x, y) {
    this.x = x;
    this.y = y;
    this.id = id;
}