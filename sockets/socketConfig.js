module.exports = {
    gameEvens: {
        EVENT_READY: 'ready',
        EVENT_ROOM_FILLED: 'filled',
        EVENT_WIN: 'win',
        EVENT_ROOM_CONNECTION: 'roomConnection',
        EVENT_DISCONNECT_FROM_ROOM: "roomDisconnection",
        EVENT_TEST : 'test'
    },
    playerEvents: {
        EVENT_GET_PLAYERS: 'getPlayers',
        EVENT_NEW_USER: 'new_user',
        EVENT_MOVED: 'moved',
        EVENT_DISCONNECT: "player_Disc"
    }
};
