const passport = require('passport');
const pStrategy = require('passport-jwt').Strategy;
const {ExtractJwt} = require('passport-jwt');
const {secret} = require('./configs/tokenSecretconfig');
const LocalStrategy = require('passport-local').Strategy;
const User = require('./models/userModel');

passport.use(new pStrategy({
    jwtFromRequest: ExtractJwt.fromUrlQueryParameter('token'),
    secretOrKey: 'tokenSecret',
}, async (payload, done) => {
    try {
        const user = await User.findById(payload.sub);
        if (!user) {
            return done(null, false)
        }
        return done(null, user);
        // should return user from mongo db with id if it exist
    } catch (error) {
        done(error, false);
    }

}));
passport.use(new LocalStrategy(
    {usernameField: 'email'
    },
    async (email, password, done) => {
        try {
            const user = await User.findOne({email});
            if (!user) {
                return done(null, false);
            }
            console.log("a");
            const correct = await user.checkValidPassword(password);
            if (!correct) {
                return done(null, false)
            }
            return done(null, user);
        } catch (e) {
            done(e, false)
        }
    }
));