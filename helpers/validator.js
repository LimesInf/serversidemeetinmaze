const Joi=require("joi");
module.exports={
  validate: (schema)=>{
      return(req,res,next)=>{
        const validationData={
           email:req.body.email,
           password: req.body.password
        };
        const result=Joi.validate(validationData,schema);
        if(result.error){
            return res.status(500).json({error:"wrong email  / password"});
        }
          if(!req.value){req.value={}};
        req.value['body']=req.body;
        next();
      }
  },
  schemas:{
    authSchma:Joi.object().keys({
       email:Joi.string().email().required(),
       password:Joi.string().required()
    })
  }
};