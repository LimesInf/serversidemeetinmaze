const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt=require('bcryptjs');

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
    },
    nick: String,
    winCount: Number,
    loseCount: Number,
    drawCount: Number,
    level: Number,
    cash:Number,
    onlineTime:Number
});
userSchema.pre('save',async  function(next) {
    try {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(this.password,salt);
        this.password=hash;
        next();
    } catch (error) {
        next(error);
    }
});
userSchema.methods.checkValidPassword = async  function(newPassword) {
  try{
      return await bcrypt.compare(newPassword,this.password);
  }  catch (e) {
      throw  new  Error(e);
  }
};

const User = mongoose.model('user', userSchema);
module.exports = User;